<?php

namespace Drupal\sessionless\Element;

use Drupal\Core\Render\Annotation\FormElement;
use Drupal\Core\Render\Element\Hidden;

/**
 * Signed data.
 *
 * Specify either #default_value or #value but not both.
 *
 * To go sessionless, it looks like only #default_value makes sense, as #value
 * relies on FormState.
 *
 * @FormElement("sessionless_signed_encrypted")
 */
class SignedEncryptedData extends Hidden {

  use SessionlessElementTrait;

  protected static function mustEncrypt(): bool {
    return TRUE;
  }

}
