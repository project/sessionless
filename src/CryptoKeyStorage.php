<?php

namespace Drupal\sessionless;

use Drupal\Core\State\StateInterface;
use Jose\Component\Core\JWK;
use Jose\Component\KeyManagement\JWKFactory;

/**
 * Key Storage
 *
 * @internal
 *
 * Notes:
 * - ECDSA signing creates smaller signatures.
 *   https://auth0.com/blog/json-web-token-signing-algorithms-overview/
 * - RSA-OAEP-256 is one of the "remaining" asymmetric algorithms.
 *   https://connect2id.com/products/nimbus-jose-jwt/examples/jwt-with-rsa-encryption
 */
final class CryptoKeyStorage {

  protected StateInterface $state;

  public function __construct(StateInterface $state) {
    $this->state = $state;
  }


  public function getSignatureKey(): JWK {
    $stateKey = "sessionless.key.signature";
    $key = $this->state->get($stateKey);
    if (!$key) {
      $key = JWKFactory::createECKey('P-256');
      $this->state->set($stateKey, $key);
    }
    return $key;
  }

  public function getEncryptionKey(): JWK {
    $stateKey = "sessionless.key.encryption";
    $key = $this->state->get($stateKey);
    if (!$key) {
      $key = JWKFactory::createRSAKey(4096);
      $this->state->set($stateKey, $key);
    }
    return $key;
  }

}
