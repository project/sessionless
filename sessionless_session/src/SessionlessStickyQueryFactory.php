<?php

namespace Drupal\sessionless_session;

use Drupal\sessionless\CryptoService;
use Drupal\sticky_query\StickyQuery\StickyQueryAppDrivenValueHandler;
use Drupal\sticky_query\StickyQuery\StickyQueryHandlerFactoryInterface;
use Drupal\sticky_query\StickyQuery\StickyQueryHandlerRegistry;
use Drupal\sticky_query\StickyQueryStorage\StickyQuerySimpleStorage;

class SessionlessStickyQueryFactory implements StickyQueryHandlerFactoryInterface {

  protected CryptoService $cryptoService;

  protected StickyQuerySimpleStorage $storage;

  public function __construct(CryptoService $cryptoService, StickyQuerySimpleStorage $storage) {
    $this->cryptoService = $cryptoService;
    $this->storage = $storage;
  }

  public function registerHandlers(StickyQueryHandlerRegistry $registry): void {
    $storage = new StickyQueryStorageEncryptionDecorator(
     $this->storage,
     $this->cryptoService
    );
    $registry->add(new StickyQueryAppDrivenValueHandler('sessionless', $storage));
  }

}
