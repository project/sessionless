<?php

namespace Drupal\sessionless_session;

use Drupal\sessionless\CryptoService;
use Drupal\sticky_query\StickyQueryStorage\StickyQueryStorageInterface;

/**
 * StickyQueryDecorator that signs and encrypts.
 */
final class StickyQueryStorageEncryptionDecorator implements StickyQueryStorageInterface {

  protected StickyQueryStorageInterface $decorated;

  protected CryptoService $cryptoService;

  protected bool $mustEncrypt;

  /**
   * @param \Drupal\sticky_query\StickyQueryStorage\StickyQueryStorageInterface $decorated
   * @param \Drupal\sessionless\CryptoService $cryptoService
   * @param bool $mustEncrypt
   */
  public function __construct(StickyQueryStorageInterface $decorated, CryptoService $cryptoService, bool $mustEncrypt = TRUE) {
    $this->decorated = $decorated;
    $this->cryptoService = $cryptoService;
    $this->mustEncrypt = $mustEncrypt;
  }

  public function getValue() {
    $data = $this->decorated->getValue();
    if ($data) {
      $jwtString = $this->cryptoService->dump($data, $this->mustEncrypt);
    }
    else {
      $jwtString = NULL;
    }
    return $jwtString;
  }

  public function setValue($value): void {
    if ($value && is_string($value)) {
      $this->decorated->setValue($this->cryptoService->parse($value, $this->mustEncrypt));
    }
    else {
      $this->decorated->setValue(NULL);
    }
  }

}
