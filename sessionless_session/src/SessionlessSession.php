<?php

namespace Drupal\sessionless_session;

use Drupal\sticky_query\StickyQueryStorage\StickyQueryStorageInterface;
use Symfony\Component\HttpFoundation\Session\SessionBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MetadataBag;

/**
 * A (partially crippled) fake session using signed and encrypted sticky query.
 */
class SessionlessSession implements SessionInterface {

  protected StickyQueryStorageInterface $storage;

  protected bool $encrypt;

  public function __construct(StickyQueryStorageInterface $storage, bool $encrypt = TRUE) {
    $this->storage = $storage;
    $this->encrypt = $encrypt;
  }


  public function start(): bool {
    return true;
  }

  public function getId(): string {
    return 'sessionless';
  }

  public function setId($id) {}

  public function getName(): string {
    return 'sessionless';
  }

  public function setName($name) {}

  public function invalidate($lifetime = NULL): bool {
    $this->storage->setValue(NULL);
    return true;
  }

  public function migrate($destroy = FALSE, $lifetime = NULL): bool {
    return true;
  }

  public function save() {}

  public function has($name): bool {
    $array = $this->storage->getValue();
    return isset($array[$name]);
  }

  public function get($name, $default = NULL): mixed {
    $array = $this->storage->getValue();
    return $array[$name] ?? $default;
  }

  public function set($name, $value) {
    $array = $this->storage->getValue();
    $array[$name] = $value;
    $this->storage->setValue($array);
  }

  public function all(): array {
    return $this->storage->getValue();
  }

  public function replace(array $attributes) {
    $array = $this->storage->getValue();
    $array = $attributes + $array;
    $this->storage->setValue($array);
  }

  public function remove($name): mixed {
    $array = $this->storage->getValue();
    $value = $array[$name] ?? NULL;
    unset($array[$name]);
    $this->storage->setValue($array);
    return $value;
  }

  public function clear() {
    $this->storage->setValue(NULL);
  }

  public function isStarted(): bool {
    return TRUE;
  }

  public function registerBag(SessionBagInterface $bag) {
    throw new \BadMethodCallException(sprintf('Method %s not allowed.', __METHOD__));
  }

  public function getBag($name): SessionBagInterface {
    throw new \BadMethodCallException(sprintf('Method %s not allowed.', __METHOD__));
  }

  public function getMetadataBag(): MetadataBag {
    throw new \BadMethodCallException(sprintf('Method %s not allowed.', __METHOD__));
  }

}
