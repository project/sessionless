<?php

namespace Drupal\sessionless_forms;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

class SessionlessFormsServiceProvider extends ServiceProviderBase {

  public function alter(ContainerBuilder $container) {
    // We can't decorate form_cache as private service, so swap out argument.
    $container->getDefinition('form_builder')
      ->replaceArgument(2, new Reference('sessionless_forms.form_cache'));
  }

}
